# Base Dockerized Angular

> UPDATE: Now using the `17.0.0` version

This project aims to give a ready to go [Angular(17.0.0)](https://angular.dev/) project with Docker implementation.  
This repo comes with [Bootstrap](https://getbootstrap.com/docs/5.3/getting-started/download/#npm) implementation (_**not ng-bootstrap**_)

## Pre requirement
In order to launch this project you'll need those requirements:
- [Docker](https://docs.docker.com/engine/install/) (and its plugin [docker-compose](https://docs.docker.com/compose/install/))
- Node ([Linux](https://nodejs.org/en/download/package-manager) - [Windows](https://nodejs.org/en/download))

## Get Started

1. Clone the repo either with **ssh** or **https** (not recommended)  
    SSH
    ```shell
    git clone git@gitlab.com:savini_rs/base-dockerized-angular.git
    ``` 
    HTTPS
    ```shell
    git clone https://gitlab.com/savini_rs/base-dockerized-angular.git
    ```
2. Symbolic link the `devel.yaml` and `common.yaml` file
    ```shell
   ln -s common.yaml docker-compose.yaml
   ln -s devel.yaml docker-compose-override.yaml
   ```

3. Start the project
    ```shell
   docker compose build
   docker compose up
   ```
   
The page is available at http://localhost:4200